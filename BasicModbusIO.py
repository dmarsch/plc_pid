#!/usr/bin/env python

#
# Based on Pymodbus Synchronous Client Example.
# Now barely any of the original remains.
#
# Does memory I/O operations with the Schneider TM221C16R in the PLC lab.
#
# B.Noble 20211028
#


#
# import the client implementation
#
from pymodbus.client.sync import ModbusTcpClient as ModbusClient

#
# configure the client logging
#
import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

import time

import PySimpleGUI as sg

# import numpy as np
# from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
# import PySimpleGUI as sg
# import matplotlib

# matplotlib.use("TkAgg")

# def draw_figure(canvas, figure):
#     figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
#     figure_canvas_agg.draw()
#     figure_canvas_agg.get_tk_widget().pack(side="top", fill="both", expand=1)
#     return figure_canvas_agg

def get_adc(temp):
    m = (818 - 204)/(90 - 50) #818 & 204 are ADC for 90F (8VDC) & 50F (2VDC)
    b = 204 - (m * 50)
    y = m * temp + b
    return int(y)
   
def get_fahrenheit(adc): #want to make adc value a float when passing in argument
    m = (90 - 50)/(818 - 204)
    b = 50 - (m * 204)
    y = (m * adc) + b
    return y

def f_to_c(f_temp):
    return  (f_temp - 32.0) * (5.0/9.0)

def f_to_k(f_temp):
    return 273.5 + ((f_temp - 32.0) * (5.0/9.0))

#
# Modbus unit to talk to
#
MODBUS_ADDR = "192.168.8.203" # IP address of PLC
MODBUS_PORT = 502

def run_sync_client():

    global temp_sel, temp_units, temp_val_tuple, alarm_threshold
    bool2binary = lambda a : 1 if a else 0

    count = 0

    #
    # Connect to the PLC
    #
    client = ModbusClient(MODBUS_ADDR, port=MODBUS_PORT)
    client.connect()
    log.info(client)

    #
    # Print an info message
    #
    log.info("Assert %I0.0 on PLC to exit this program.")

    #
    # Put our addresses in variables so we can reference them by a meaningful name
    #
    M0ADDR = 0;
    M1ADDR = 1;
    M2ADDR = 2;
    M3ADDR = 3;
    M4ADDR = 4;
    M5ADDR = 5;
    M6ADDR = 6;
    M7ADDR = 7;
    M8ADDR = 8;
    M9ADDR = 9;
    MW0ADDR = 0;
    MW1ADDR = 1;
    MW2ADDR = 2;
    MW3ADDR = 3;
    
    temp_units = [u'\N{DEGREE SIGN}F', u'\N{DEGREE SIGN}C', 'K'] #define units
    temp_sel = 0
    elapsed_time = 0
    turn_off_sel_coil = 0
    fan_mode = 0
    heater_state = 0
    heater_text = 'OFF'
    
    window_open = 0
    
    #initialize deadband to 1F
    deadband_threshold = 1

    # Initialize Alarm Temperature
    alarm_threshold = float(85) #this should be a float
    alarm_adc_threshold = get_adc(alarm_threshold)
    rr = client.write_register(MW0ADDR, alarm_adc_threshold) #write 85F ADC as initial value
    assert not rr.isError()

    time.sleep(1)

    while True:
        # start = time.time()
        
        while True:
            rr = client.read_coils(M9ADDR, 1) #read temperature select button
            assert not rr.isError()
            if rr.bits[0]:
                turn_off_sel_coil = 1
                temp_sel += 1
                if (temp_sel % 3 == 0): #go back to Fahrenheit
                    temp_sel = 0
                break
            else:
                # log.info("temperature select hasn't changed")
                break
            
        while True:
            rr = client.read_coils(M1ADDR, 1) #read temperature threshold coil
            assert not rr.isError()
            if rr.bits[0]:
                heater_state = 1
                break
            else:
                heater_state = 0
                break
            
        if turn_off_sel_coil == 1:
            turn_off_sel_coil = 0
            rq = client.write_coil(M9ADDR, int(0)) #turn off the temp select input since it's been read
            assert not rq.isError()
            
        if fan_mode == 1 and heater_state == 1: #Fan on mode so turn on M2 coil (heater off because threshold met)
            rq = client.write_coil(M2ADDR, fan_mode)
            assert not rq.isError()
        elif fan_mode == 0 and heater_state == 1: #turn off fan coil since heater should be off
            rq = client.write_coil(M2ADDR, fan_mode)
            assert not rq.isError()
                
        # if (elapsed_time >= 1):
        #     elapsed_time = 0 #clear this
        while True:
            rr = client.read_coils(M1ADDR, 1) #read high temp alarm coil
            assert not rr.isError()
            if rr.bits[0]:
                log.info("HIGH TEMP ALERT")
                break
            else:
                log.info("temperatures are normal")
                break
        
        # Write the alarm threshold (when to turn off the heater)
        alarm_adc_threshold = get_adc(alarm_threshold + deadband_threshold)
        rr = client.write_register(MW0ADDR, alarm_adc_threshold)
        assert not rr.isError()
        
        # Write the DB threshold (when to turn on the heater after the heater turn off from reaching setpoint + DB)
        db_adc_threshold = get_adc(alarm_threshold - deadband_threshold)
        rr = client.write_register(MW3ADDR, db_adc_threshold)
        assert not rr.isError()
            
        #
        # Read the values of the output register holding the ADC value
        #
        log.info(f"Reading ADC value from %MW2")
        rr = client.read_holding_registers(MW2ADDR, 1)
        assert not rr.isError()
        adc_value = rr.registers[0]
        log.info(f"ADC value = {adc_value}")
        temp_value_f = get_fahrenheit(float(adc_value)) #get the F of the potentiometer ADC
        temp_value_c = f_to_c(float(temp_value_f)) #convert to C
        temp_value_k = f_to_k(float(temp_value_f)) #convert to K
        temp_val_tuple = [temp_value_f, temp_value_c, temp_value_k] #create tuple to increment through
        
        log.info(f"Temperature value = {temp_val_tuple[temp_sel]}{temp_units[temp_sel]}") #display correct conversion
        
        #setting up GUI
        layout = [[sg.Text('Current Temperature:'), sg.Text(size=(12,1), key = 'current')],
          [sg.Text('Temperature Setpoint:'), sg.Text(size=(12,1), key = 'setpoint')],
          [sg.Text('Deadband Setpoint:'), sg.Text(size=(12,1))],
          [sg.Slider(range=(1, 5), orientation='h', size=(20, 5), default_value=1, key = 'slider')],
          [sg.Button('SP Up')], #button for increasing threshold
          # [sg.Button('SP Down')]]
          [sg.Button('SP Down')],
          [sg.Combo(['Fan AUTO', 'Fan ON'], default_value='Fan AUTO', key = 'combo')],
          [sg.Text('Heater: '), sg.Text(size=(12,1), key = 'heater')]]
         
        if window_open == 0: #don't want to keep opening windows
           window = sg.Window('Temperature Window', layout)
           window_open = 1
        
        #User pressed a button in GUI
        event, values = window.read(timeout = 100)
        if event in (sg.WIN_CLOSED, 'Exit'):
            break
        elif event == 'SP Up':
            alarm_threshold = alarm_threshold + 1
        elif event == 'SP Down':
            alarm_threshold = alarm_threshold - 1
        
        fan_combo = values['combo']
        if fan_combo == 'Fan ON':
            fan_mode = 1 #turn on M2 coil to keep fan on
        elif fan_combo == 'Fan AUTO':
            fan_mode = 0
            
        if heater_state == 0:
            heater_text = 'ON'
        elif heater_state == 1:
            heater_text = 'OFF'
            
        #Update GUI with new values in window
        window['setpoint'].update(alarm_threshold)
        window['current'].update(f'{temp_val_tuple[temp_sel]: .2f}{temp_units[temp_sel]}')
        window['slider'].update(values['slider'])
        deadband_threshold = values['slider']
        window['heater'].update(heater_text)
        

            
            
        # stop = time.time()
        # elapsed_time += (stop - start) #add the time
        # #
        # # Wait one second
        # #
        time.sleep(1)
   
    #close the GUI window
    window.close()
    #
    # close the client
    #
    client.close()
    log.info(f"Exiting")


if __name__ == "__main__":
    run_sync_client()
